// This file was generated by gir (https://github.com/gtk-rs/gir)
// from ../ev-girs
// from ../gir-files
// DO NOT EDIT

use crate::ffi;
use glib::translate::*;

glib::wrapper! {
    #[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
    pub struct Bookmark(Boxed<ffi::PpsBookmark>);

    match fn {
        copy => |ptr| ffi::pps_bookmark_copy(ptr),
        free => |ptr| ffi::pps_bookmark_free(ptr),
        type_ => || ffi::pps_bookmark_get_type(),
    }
}

impl Bookmark {
    #[doc(alias = "pps_bookmark_new")]
    pub fn new(page: i32, title: &str) -> Bookmark {
        assert_initialized_main_thread!();
        unsafe { from_glib_full(ffi::pps_bookmark_new(page, title.to_glib_none().0)) }
    }

    #[doc(alias = "pps_bookmark_get_page")]
    #[doc(alias = "get_page")]
    pub fn page(&self) -> u32 {
        unsafe { ffi::pps_bookmark_get_page(self.to_glib_none().0) }
    }

    #[doc(alias = "pps_bookmark_get_title")]
    #[doc(alias = "get_title")]
    pub fn title(&self) -> Option<glib::GString> {
        unsafe { from_glib_none(ffi::pps_bookmark_get_title(self.to_glib_none().0)) }
    }
}
